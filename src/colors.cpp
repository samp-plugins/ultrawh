#include "colors.hpp"
#include "config.hpp"

D3DCOLOR colors::data[count] = { 0 };

void colors::init() {
	auto &config = config::instance();
	for (auto i = 0; i < count; ++i) {
		if (config->skins_aztecas.contains(i)) {
			data[i] = config->color_aztecas.abgr;
		} else if (config->skins_ballas.contains(i)) {
			data[i] = config->color_ballas.abgr;
		} else if (config->skins_rifa.contains(i)) {
			data[i] = config->color_rifa.abgr;
		} else if (config->skins_vagos.contains(i)) {
			data[i] = config->color_vagos.abgr;
		} else if (config->skins_grove.contains(i)) {
			data[i] = config->color_grove.abgr;
		} else if (config->skins_lcn.contains(i)) {
			data[i] = config->color_lcn.abgr;
		} else if (config->skins_rm.contains(i)) {
			data[i] = config->color_rm.abgr;
		} else if (config->skins_yakuza.contains(i)) {
			data[i] = config->color_yakuza.abgr;
		} else {
			data[i] = -1;
		}
	}
}
