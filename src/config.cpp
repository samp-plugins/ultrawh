#include "config.hpp"
#include <filesystem>
#include <fstream>
#include <nlohmann/json.hpp>
#include "misc.hpp"

extern std::filesystem::path g_config_path;

color::color(std::uint8_t a, std::uint8_t r, std::uint8_t g, std::uint8_t b) :
	a(a), r(r), g(g), b(b) {}

color::color(std::uint32_t argb) :
	abgr(misc::argb_to_abgr(argb)) {}

config::config() = default;

config::~config() = default;

config &config::instance() {
	static config inst;
	return inst;
}

void config::load() {
	std::ifstream cfg_file(g_config_path);
	if (!cfg_file.is_open()) return save();
	auto content = std::string((std::istreambuf_iterator<char>(cfg_file)), std::istreambuf_iterator<char>());
	cfg_file.close();
	try {
		data_ = nlohmann::json::parse(content);
	} catch (...) {
		save();
	}
}

void config::save() const {
	nlohmann::json json = data_;
	// write to file
	std::ofstream(g_config_path) << json.dump(4);
}

struct config::data *config::operator->() {
	return &data_;
}

const struct config::data *config::operator->() const {
	return &data_;
}
