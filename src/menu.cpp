#include "menu.hpp"
#include <sstream>
#include "config.hpp"
#include "misc.hpp"

menu::menu(std::shared_ptr<wallhack> wh) :
	wh_(std::move(wh)) {
	auto &config = config::instance();
	options_	 = {
			// Global options
		std::make_pair("Disable SAMP nametags", &config->disable_samp_nametags),
		std::make_pair("Clear screenshot", &config->clear_screenshot),
		// Wallhack options
		std::make_pair("Show nametags", &config->wallhack_nametags),
		std::make_pair("Show bars", &config->wallhack_bars),
		std::make_pair("Show bones", &config->wallhack_bones),
		std::make_pair("Show box", &config->wallhack_boxes),
		std::make_pair("Show lines", &config->wallhack_lines)
	};
	current_option_ = 0;
	max_option_		= static_cast<int>(options_.size()) + 1;
}

menu::~menu() = default;

void menu::draw() {
	static auto &config		 = config::instance();
	static auto	 proc_option = [this](int i) {
		 if (i == current_option_) {
			 rect(200.f, 17.f, 0xFF0045FF, true, true, true);
			 draw_pos_.x += style_.item_spacing.x;
		 }
		 auto &&[text, option] = options_[i];
		 if (*option) {
			 outline_text(text, 0xFF00D7FF);
		 } else {
			 outline_text(text, 0xFFFF9500);
		 }
	};

	background(200.f, 300.f);

	outline_text("Ultra WH Settings", 0xFFBEBEBE);
	separator();

	outline_text("Global options", 0xFFBEBEBE);
	auto i = 0;
	for (; i < 2; ++i) proc_option(i);

	outline_text("Wallhack options", 0xFFBEBEBE);
	for (; i < max_option_ - 1; ++i) proc_option(i);
	if (current_option_ == option::font_size) {
		rect(200.f, 34.f, 0xFF0045FF, true, true, true);
		draw_pos_.x += style_.item_spacing.x;
	}
	progress_bar("Font size", 0xFF000000, 0xFFFF9500, 0xFF00D7FF, config->font_size, 100.f);
}

bool menu::wndproc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam) {
	if (msg == WM_KEYDOWN) {
		static auto &config = config::instance();
		if (wparam == VK_UP) {
			current_option_ = (current_option_ - 1) % max_option_;
			if (current_option_ < 0) current_option_ = max_option_ - 1;
			return false;
		} else if (wparam == VK_DOWN) {
			current_option_ = (current_option_ + 1) % max_option_;
			return false;
		} else if (wparam == VK_RETURN) {
			if (current_option_ != option::font_size) {
				*options_[current_option_].second ^= true;
				if (current_option_ == option::disable_samp_nametags) {
					misc::toggle_nicks(!(config->act && config->disable_samp_nametags));
				}
			} else {
				wh_->update_font();
			}
			config.save();
			return false;
		} else if (current_option_ == option::font_size) {
			if (wparam == VK_LEFT) {
				config->font_size -= 1.f;
				if (config->font_size < 0.f) config->font_size = 0.f;
				return false;
			} else if (wparam == VK_RIGHT) {
				config->font_size += 1.f;
				if (config->font_size > 100.f) config->font_size = 100.f;
				return false;
			}
		}
	} else if (msg == WM_KEYUP && (wparam == VK_UP ||
								   wparam == VK_DOWN ||
								   wparam == VK_RETURN ||
								   wparam == VK_LEFT ||
								   wparam == VK_RIGHT)) {
		return false;
	}

	return true;
}

void menu::background(float width, float height) {
	auto [gta_width, gta_height] = misc::screen_res();

	win_start_pos_ = ImVec2(gta_width - width - style_.border_size, gta_height - height - style_.border_size);
	win_end_pos_   = ImVec2(gta_width - style_.border_size, gta_height - style_.border_size);

	auto draw_list = ImGui::GetBackgroundDrawList();

	// [Main rect - 0x96000000]
	draw_list->AddRectFilled(win_start_pos_, win_end_pos_, style_.colors.background);

	// [Borders - 0xFFFF9500]
	// Top
	draw_list->AddRectFilled({ win_start_pos_.x - style_.border_size, win_start_pos_.y - style_.border_size },
							 { win_end_pos_.x + style_.border_size, win_start_pos_.y },
							 style_.colors.border);
	// Bottom
	draw_list->AddRectFilled({ win_start_pos_.x - style_.border_size, win_end_pos_.y },
							 { win_end_pos_.x + style_.border_size, win_end_pos_.y + style_.border_size },
							 style_.colors.border);
	// Left
	draw_list->AddRectFilled({ win_start_pos_.x - style_.border_size, win_start_pos_.y },
							 { win_start_pos_.x, win_end_pos_.y },
							 style_.colors.border);
	// Right
	draw_list->AddRectFilled({ win_end_pos_.x, win_start_pos_.y },
							 { win_end_pos_.x + style_.border_size, win_end_pos_.y },
							 style_.colors.border);

	draw_pos_.x = win_start_pos_.x + style_.item_spacing.x;
	draw_pos_.y = win_start_pos_.y + style_.item_spacing.y;
}

void menu::outline_text(std::string_view text, std::uint32_t color) {
	auto &pos		= draw_pos_;
	auto  draw_list = ImGui::GetBackgroundDrawList();

	// Border
	draw_list->AddText(ImVec2(pos.x + 1, pos.y + 1), IM_COL32_BLACK, text.data());
	draw_list->AddText(ImVec2(pos.x - 1, pos.y - 1), IM_COL32_BLACK, text.data());
	draw_list->AddText(ImVec2(pos.x + 1, pos.y - 1), IM_COL32_BLACK, text.data());
	draw_list->AddText(ImVec2(pos.x - 1, pos.y + 1), IM_COL32_BLACK, text.data());
	// Text itself
	draw_list->AddText(pos, color, text.data());

	auto text_size = ImGui::CalcTextSize(text.data());
	draw_pos_.x	   = win_start_pos_.x + style_.item_spacing.x;
	draw_pos_.y += text_size.y + 1.f + style_.item_spacing.y;
}

void menu::separator() {
	auto draw_list = ImGui::GetBackgroundDrawList();

	draw_list->AddRectFilled({ draw_pos_.x - style_.item_spacing.x, draw_pos_.y },
							 { win_end_pos_.x, draw_pos_.y + style_.border_size },
							 style_.colors.border);

	draw_pos_.x = win_start_pos_.x + style_.item_spacing.x;
	draw_pos_.y += style_.border_size + style_.item_spacing.y;
}

void menu::rect(float width, float height, std::uint32_t color, bool filled, bool is_bg, bool no_margins) {
	auto draw_list = ImGui::GetBackgroundDrawList();

	ImVec2 start;
	ImVec2 end;
	if (no_margins) {
		start = ImVec2(draw_pos_.x - style_.item_spacing.x, draw_pos_.y);
		end	  = ImVec2(draw_pos_.x - style_.item_spacing.x + width, draw_pos_.y + height);
	} else {
		start = ImVec2(draw_pos_.x, draw_pos_.y);
		end	  = ImVec2(draw_pos_.x + width, draw_pos_.y + height);
	}

	if (filled) {
		draw_list->AddRectFilled(start, end, color);
	} else {
		draw_list->AddRect(start, end, color);
	}

	if (!is_bg) {
		draw_pos_.x = win_start_pos_.x + style_.item_spacing.x;
		draw_pos_.y += height + style_.item_spacing.y;
	}
}

void menu::progress_bar(std::string_view text, std::uint32_t border_color, std::uint32_t bar_color, std::uint32_t text_color, float current, float max) {
	static const auto border_width	= 200.f - 2.f * style_.item_spacing.x - 4.f;
	static const auto border_height = 13.f;

	draw_pos_.y += 2.f;

	// border
	auto start_x = draw_pos_.x;
	rect(border_width, border_height, border_color, false, true);
	draw_pos_.x += 2.f;
	draw_pos_.y += 2.f;

	// bar itself
	auto max_width	   = border_width - 4.f;
	auto current_width = max_width * (current / max);
	rect(current_width, border_height - 4.f, bar_color, true, false);
	draw_pos_.x += 2.f;

	// text
	std::stringstream ss;
	ss << text << " = " << std::fixed << std::setprecision(2) << current << "/" << max;
	draw_pos_.x = start_x;
	outline_text(ss.str(), text_color);

	draw_pos_.x = win_start_pos_.x + style_.item_spacing.x;
	draw_pos_.y += 17.f + style_.item_spacing.y;
}