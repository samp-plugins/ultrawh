#ifndef WALLHACK_HPP
#define WALLHACK_HPP

#include <d3d9.h>
#include <imgui.h>

namespace samp {
	class remote_player;
}

class wallhack {
	ImFont							 *font_ = nullptr;

public:
	wallhack();
	~wallhack();

	void draw() const;
	void update_font();

private:
	void nametag(samp::remote_player &remote_player) const;
	void bars(samp::remote_player &remote_player) const;

	void bones(samp::remote_player &remote_player, D3DCOLOR color) const;
	void box(samp::remote_player &remote_player, D3DCOLOR color) const;
	void lines(samp::remote_player &remote_player, D3DCOLOR color) const;

};

#endif // WALLHACK_HPP
