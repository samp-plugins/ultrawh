#ifndef MENU_HPP
#define MENU_HPP

#include <cstdint>
#include <memory>
#include <string>
#include <string_view>
#include <utility>
#include <vector>
#include <windows.h>
#include <imgui.h>
#include "wallhack.hpp"

class menu {
	struct style {
		float border_size = 2.f;
		struct {
			float x = 6.f;
			float y = 2.f;
		} item_spacing;

		struct {
			std::uint32_t background = 0x96000000;
			std::uint32_t border	 = 0xFFFF9500;
		} colors;
	} style_;

	ImVec2 win_start_pos_;
	ImVec2 win_end_pos_;

	ImVec2 draw_pos_;

public:
	menu(std::shared_ptr<wallhack> wh);
	~menu();

	void draw();
	bool wndproc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);

private:
	enum option { disable_samp_nametags,
				  clear_screenshot,
				  wallhack_nametags,
				  wallhack_bars,
				  wallhack_bones,
				  wallhack_boxes,
				  wallhack_lines,
				  font_size };

	std::shared_ptr<wallhack> wh_;

	std::vector<std::pair<std::string, bool *>> options_;
	int											current_option_ = 0;
	int											max_option_		= 0;

	void background(float width, float height);
	void separator();

	void rect(float width, float height, std::uint32_t color, bool filled, bool is_bg = false, bool no_margins = false);
	void outline_text(std::string_view text, std::uint32_t color = -1);

	void progress_bar(std::string_view text, std::uint32_t border_color, std::uint32_t bar_color, std::uint32_t text_color, float current, float max = 100.f);
};

#endif // MENU_HPP
