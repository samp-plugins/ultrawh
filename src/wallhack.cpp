#include "wallhack.hpp"
#include <unordered_map>
#include <backends/imgui_impl_dx9.h>
#include <imgui.h>
#include "colors.hpp"
#include "config.hpp"
#include "misc.hpp"
#include "samp/base.hpp"
#include "samp/pools.hpp"
#include "samp/player_pool.hpp"
#include "samp/remote_player.hpp"
#include "tahoma.hpp"

wallhack::wallhack() {
	update_font();
}

wallhack::~wallhack() = default;

void wallhack::draw() const {
	static auto &config = config::instance();

	for (auto i = 0; i < samp::player_pool::max_player; ++i) {
		auto &player_pool = samp::base::instance().pools().player_pool();
		if (i == player_pool.local_player_id() || !player_pool.exists(i)) continue;

		auto remote_player = player_pool.remote_player(i);

		if (config->wallhack_nametags) nametag(remote_player);
		if (config->wallhack_bars) bars(remote_player);

		auto color = colors::data[remote_player.skin()];
		if (config->wallhack_bones) bones(remote_player, color);
		if (config->wallhack_boxes) box(remote_player, color);
		if (config->wallhack_lines) lines(remote_player, color);
	}
}

void wallhack::update_font() {
	ImGui_ImplDX9_InvalidateDeviceObjects();
	auto &config = config::instance();

	ImFontConfig font_config;
	font_config.GlyphExtraSpacing.x = 1.f;

	auto &io = ImGui::GetIO();
	font_	 = io.Fonts->AddFontFromMemoryCompressedBase85TTF(tahoma_compressed_data_base85, config->font_size, &font_config);
}

void wallhack::nametag(samp::remote_player &remote_player) const {
	auto screen_bone_pos = misc::world_to_screen_pos(misc::get_bone_pos(remote_player.gta_ped(), 6));
	if (screen_bone_pos.z < 1.f) return;
	auto id		 = remote_player.id();
	auto wh_name = remote_player.name() + "[" + std::to_string(id) + "]";
	auto pos	 = ImVec2(screen_bone_pos.x - 50.f, screen_bone_pos.y - 65.f);

	auto draw_list = ImGui::GetBackgroundDrawList();

	draw_list->AddText(font_, font_->FontSize, ImVec2(pos.x + 1, pos.y + 1), IM_COL32_BLACK, wh_name.c_str());
	draw_list->AddText(font_, font_->FontSize, ImVec2(pos.x - 1, pos.y - 1), IM_COL32_BLACK, wh_name.c_str());
	draw_list->AddText(font_, font_->FontSize, ImVec2(pos.x + 1, pos.y - 1), IM_COL32_BLACK, wh_name.c_str());
	draw_list->AddText(font_, font_->FontSize, ImVec2(pos.x - 1, pos.y + 1), IM_COL32_BLACK, wh_name.c_str());

	draw_list->AddText(font_, font_->FontSize, pos, misc::argb_to_abgr(misc::samp_clist_color(id)), wh_name.c_str());
}

void wallhack::bars(samp::remote_player &remote_player) const {
	auto screen_bone_pos = misc::world_to_screen_pos(misc::get_bone_pos(remote_player.gta_ped(), 6));
	if (screen_bone_pos.z < 1.f) return;

	auto hp = remote_player.health();
	auto ap = remote_player.armour();
	if (hp > 100.f) hp = 100.f;
	if (ap > 100.f) ap = 100.f;

	auto start_border_pos = ImVec2(screen_bone_pos.x - 51.f, screen_bone_pos.y - 46.f);
	auto end_border_pos	  = ImVec2(screen_bone_pos.x - 51.f + 52.f, screen_bone_pos.y - 46.f + 7.f);
	auto start_ind_pos	  = ImVec2(screen_bone_pos.x - 50.f, screen_bone_pos.y - 45.f);
	auto end_ind_pos	  = ImVec2(screen_bone_pos.x - 50.f + hp / 2.f, screen_bone_pos.y - 45.f + 5.f);

	auto draw_list = ImGui::GetBackgroundDrawList();

	draw_list->AddRectFilled(start_border_pos, end_border_pos, 0x55000000);
	draw_list->AddRectFilled(start_ind_pos, end_ind_pos, 0xFF0000FF);
	if (ap > 0.f) {
		start_border_pos = ImVec2(screen_bone_pos.x - 51.f, screen_bone_pos.y - 38.f);
		end_border_pos	 = ImVec2(screen_bone_pos.x - 51.f + 52.f, screen_bone_pos.y - 38.f + 7.f);
		start_ind_pos	 = ImVec2(screen_bone_pos.x - 50.f, screen_bone_pos.y - 37.f);
		end_ind_pos		 = ImVec2(screen_bone_pos.x - 50.f + ap / 2.f, screen_bone_pos.y - 37.f + 5.f);

		draw_list->AddRectFilled(start_border_pos, end_border_pos, 0x55000000);
		draw_list->AddRectFilled(start_ind_pos, end_ind_pos, 0xFFF0F0F0);
	}
}

void wallhack::bones(samp::remote_player &remote_player, D3DCOLOR color) const {
	static const std::unordered_map<int, int> bones_pair = {
		{53, 52},
		{51, 52},
		{43, 42},
		{41, 42},
		{51,  2},
		{41,  2},
		{ 3,	 2},
		{ 4,	 3},
		{ 4, 22},
		{ 4, 32},
		{32, 33},
		{22, 23},
		{23, 24},
		{33, 34},
		{ 4,	 8},
		{ 8,	 7},
		{ 8,	 6},
		{ 6,	 7}
	};

	auto draw_list = ImGui::GetBackgroundDrawList();
	for (auto &&[start_bone_id, end_bone_id] : bones_pair) {
		auto start_bone_pos = misc::world_to_screen_pos(misc::get_bone_pos(remote_player.gta_ped(), start_bone_id));
		if (start_bone_pos.z < 1.f) return;

		auto end_bone_pos = misc::world_to_screen_pos(misc::get_bone_pos(remote_player.gta_ped(), end_bone_id));
		if (end_bone_pos.z < 1.f) return;

		draw_list->AddLine({ start_bone_pos.x, start_bone_pos.y }, { end_bone_pos.x, end_bone_pos.y }, color);
	}
}

void wallhack::box(samp::remote_player &remote_player, D3DCOLOR color) const {
	static constexpr vector movers[] = {
	// top
		{ 0.3f,	0.3f,  0.3f},
		{-0.3f,	 0.3f,  0.3f},
		{-0.3f, -0.3f,	0.3f},
		{ 0.3f, -0.3f,  0.3f},
 // bottom
		{ 0.3f,	0.3f, -1.7f},
		{-0.3f,	 0.3f, -1.7f},
		{-0.3f, -0.3f, -1.7f},
		{ 0.3f, -0.3f, -1.7f}
	};

	auto   world_bone_pos = misc::get_bone_pos(remote_player.gta_ped(), 8);
	vector pos[8];
	for (int i = 0; i < 8; ++i) {
		pos[i] = misc::world_to_screen_pos(world_bone_pos + movers[i]);
		if (pos[i].z < 1.f) return;
	}

	auto draw_list = ImGui::GetBackgroundDrawList();
	for (int i = 0; i < 4; ++i) draw_list->AddLine({ pos[i].x, pos[i].y }, { pos[(i + 1) % 4].x, pos[(i + 1) % 4].y }, color);
	for (int i = 0; i < 4; ++i) draw_list->AddLine({ pos[i].x, pos[i].y }, { pos[i + 4].x, pos[i + 4].y }, color);
	for (int i = 4; i < 8; ++i) draw_list->AddLine({ pos[i].x, pos[i].y }, { pos[(i + 1) % 4 + 4].x, pos[(i + 1) % 4 + 4].y }, color);
}

void wallhack::lines(samp::remote_player &remote_player, D3DCOLOR color) const {
	auto bone_world_pos	 = misc::get_bone_pos(remote_player.gta_ped(), 2);
	auto bone_screen_pos = misc::world_to_screen_pos(bone_world_pos);
	if (bone_screen_pos.z < 1.f) return;

	auto [x, y, z]			  = samp::base::instance().pools().player_pool().local_player_pos();
	auto local_ped_screen_pos = misc::world_to_screen_pos({ x, y, z });
	if (local_ped_screen_pos.z < 1.f) return;

	auto draw_list = ImGui::GetBackgroundDrawList();
	draw_list->AddLine({ local_ped_screen_pos.x, local_ped_screen_pos.y }, { bone_screen_pos.x, bone_screen_pos.y }, color);
}
