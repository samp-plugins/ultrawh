#ifndef PROTOTYPES_HPP
#define PROTOTYPES_HPP

#include <d3d9.h>
#include <windows.h>

using wndproc_t = LRESULT(CALLBACK *)(HWND, UINT, WPARAM, LPARAM);

using present_t = HRESULT(__stdcall *)(LPDIRECT3DDEVICE9, CONST RECT *, CONST RECT *, HWND, CONST RGNDATA *);
using reset_t	= HRESULT(__stdcall *)(LPDIRECT3DDEVICE9, D3DPRESENT_PARAMETERS *);

using take_screenshot_t = void(__cdecl *)();

#endif // PROTOTYPES_HPP
