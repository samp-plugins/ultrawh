#ifndef MISC_HPP
#define MISC_HPP

#include <cstdint>
#include <tuple>
#include <d3d9.h>
#include "samp/lib.hpp"

class vector {
public:
	float x = 0.f;
	float y = 0.f;
	float z = 0.f;

	constexpr vector(float x = 0.f, float y = 0.f, float z = 0.f) :
		x(x), y(y), z(z) {}
	constexpr vector(const vector &vector) = default;
	constexpr vector(vector &&vector)		 = default;

	constexpr vector &operator=(const vector &vector) = default;
	constexpr vector &operator=(vector &&vector)		= default;

	constexpr vector &operator+=(const vector &v) {
		x += v.x;
		y += v.y;
		z += v.z;
		return *this;
	}
	constexpr vector &operator-=(const vector &v) {
		x -= v.x;
		y -= v.y;
		z -= v.z;
		return *this;
	}

	constexpr vector operator+(const vector &other) const {
		vector left = *this;
		left += other;
		return left;
	}
	constexpr vector operator-(const vector &other) const {
		vector left = *this;
		left -= other;
		return left;
	}
};

namespace misc {
	vector get_bone_pos(void *ped, int bone_id);
	vector world_to_screen_pos(const vector &world_pos);

	D3DCOLOR samp_clist_color(int id);
	D3DCOLOR argb_to_abgr(D3DCOLOR argb);

	std::tuple<float, float> screen_res();

	void toggle_nicks(bool status);
} // namespace misc

#endif // MISC_HPP
