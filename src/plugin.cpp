#include "plugin.hpp"
#include <imgui.h>
#include <backends/imgui_impl_dx9.h>
#include <backends/imgui_impl_win32.h>
#include "colors.hpp"
#include "config.hpp"
#include "misc.hpp"
#include "tahoma.hpp"

extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

plugin::plugin() {
	auto &config = config::instance();
	config.load();
	colors::init();

	ImGui::CreateContext();
	ImGui_ImplWin32_Init(**reinterpret_cast<HWND **>(0xC17054));
	ImGui_ImplDX9_Init(dx9::device());
	auto &style					 = ImGui::GetStyle();
	style.AntiAliasedLines		 = false;
	style.AntiAliasedFill		 = false;
	style.AntiAliasedLinesUseTex = false;
	ImFontConfig font_config;
	font_config.GlyphExtraSpacing.x = 1.f;
	auto &io						= ImGui::GetIO();
	io.Fonts->AddFontFromMemoryCompressedBase85TTF(tahoma_compressed_data_base85, 16.f, &font_config);
	io.IniFilename = nullptr;

	wh_	  = std::make_shared<wallhack>();
	menu_ = std::make_shared<menu>(wh_);

	wndproc_hook_.install(std::make_tuple(this, &plugin::wndproc));

	auto present_addr	= present_hook_.addr();
	auto present_hooked = (*reinterpret_cast<std::uint8_t *>(present_addr) & 0xF0) == 0xE0;
	if (present_hooked) present_hook_.set_addr(present_addr + 0x5);
	present_hook_.on_before += std::make_tuple(this, &plugin::on_present);
	present_hook_.install(present_hooked ? 8 : 4);

	auto reset_addr	  = reset_hook_.addr();
	auto reset_hooked = (*reinterpret_cast<std::uint8_t *>(reset_addr) & 0xF0) == 0xE0;
	if (reset_hooked) reset_hook_.set_addr(reset_addr + 0x5);
	reset_hook_.on_before += std::make_tuple(this, &plugin::on_reset);
	reset_hook_.install(reset_hooked ? 8 : 4);

	if (samp::is_r3()) take_screenshot_hook_.set_addr(0x74EB0);
	take_screenshot_hook_.install(std::make_tuple(this, &plugin::take_screenshot));
}

plugin::~plugin() {
	config::instance().save();

	wndproc_hook_.remove();
	present_hook_.remove();
	reset_hook_.remove();

	ImGui_ImplDX9_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();
}

LRESULT plugin::wndproc(wndproc_t orig, HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam) {
	static auto &config = config::instance();

	if (config->menu && !menu_->wndproc(hwnd, msg, wparam, lparam)) {
		return 1;
	}

	if (msg == WM_KEYUP) {
		if (wparam == config->act_key) {
			config->act ^= true;
			misc::toggle_nicks(!(config->act && config->disable_samp_nametags));
		} else if (wparam == config->menu_key) {
			config->menu ^= true;
		}
	}
	return orig(hwnd, msg, wparam, lparam);
}

void plugin::on_present() {
	static auto &config = config::instance();

	static int screenshot_frame_count = 0;
	if (screenshot_) {
		if (screenshot_frame_count++ == 0) {
			misc::toggle_nicks(true);
			return;
		}
		take_screenshot_hook_.orig_func()();

		misc::toggle_nicks(!(config->act && config->disable_samp_nametags));
		screenshot_			   = false;
		screenshot_frame_count = 0;
	}

	ImGui_ImplDX9_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();

	ImGui::SetMouseCursor(ImGuiMouseCursor_None);
	if (config->act) wh_->draw();
	if (config->menu) menu_->draw();

	ImGui::EndFrame();
	ImGui::Render();
	ImGui_ImplDX9_RenderDrawData(ImGui::GetDrawData());
}

void plugin::on_reset() {
	ImGui_ImplDX9_InvalidateDeviceObjects();
}

void plugin::take_screenshot(take_screenshot_t orig) {
	auto &config = config::instance();
	if (config->clear_screenshot && (config->act || config->menu)) {
		misc::toggle_nicks(true);
		screenshot_ = true;
		return;
	}
	return orig();
}
