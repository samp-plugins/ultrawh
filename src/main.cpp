#include <filesystem>
#include <lemon/hook.hpp>
#include "plugin.hpp"
#include "samp/base.hpp"
#include "samp/lib.hpp"

std::unique_ptr<plugin> plug;

std::filesystem::path g_config_path;

void gameloop() {
	static bool init = false;
	if (init || !dx9::find_device() || !samp::inited()) return;
	plug = std::make_unique<plugin>();
	init = true;
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD dwReasonForCall, LPVOID) {
	if (samp::ver() == samp::ver::unknown) return FALSE;
	static lemon::hook<> gameloop_hook(0x748DA3);
	if (dwReasonForCall == DLL_PROCESS_ATTACH) {
		char mod_path[MAX_PATH] = { 0 };
		GetModuleFileNameA(hModule, mod_path, MAX_PATH);
		g_config_path = std::filesystem::path(mod_path).replace_extension("json");

		gameloop_hook.on_before += &gameloop;
		gameloop_hook.install();
	}
	return TRUE;
}