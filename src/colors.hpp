#ifndef COLORS_HPP
#define COLORS_HPP

#include <d3d9.h>

class colors {
public:
	static constexpr auto count = 312;
	static D3DCOLOR		  data[count];

	static void init();
};

#endif // COLORS_HPP
