#ifndef CONFIG_HPP
#define CONFIG_HPP

#include <cstdint>
#include <set>
#include <nlohmann/json.hpp>

union color {
	struct {
		std::uint8_t r;
		std::uint8_t g;
		std::uint8_t b;
		std::uint8_t a;
	};
	std::uint32_t abgr;

	color(std::uint8_t a, std::uint8_t r, std::uint8_t g, std::uint8_t b);
	color(std::uint32_t argb);

	NLOHMANN_DEFINE_TYPE_INTRUSIVE(color, a, r, g, b);
};

class config {
public:
	struct data {
		bool act  = false;
		bool menu = false;

		// Global
		std::uint16_t act_key				= 116;
		std::uint16_t menu_key				= 115;
		bool		  disable_samp_nametags = true;
		bool		  clear_screenshot		= true;

		// Wallhack
		bool  wallhack_nametags = true;
		bool  wallhack_bars		= true;
		bool  wallhack_bones	= true;
		bool  wallhack_boxes	= true;
		bool  wallhack_lines	= true;
		float font_size			= 16.f;

		// Skins
		std::set<std::uint16_t> skins_aztecas = { 114, 115, 116, 41, 292 };
		std::set<std::uint16_t> skins_ballas  = { 102, 103, 104, 195 };
		std::set<std::uint16_t> skins_rifa	  = { 173, 174, 175, 226 };
		std::set<std::uint16_t> skins_vagos	  = { 190, 108, 109, 110 };
		std::set<std::uint16_t> skins_grove	  = { 105, 106, 107, 269, 270, 271, 195 };

		std::set<std::uint16_t> skins_lcn	 = { 91, 123, 124, 113 };
		std::set<std::uint16_t> skins_rm	 = { 111, 112, 214, 125, 272 };
		std::set<std::uint16_t> skins_yakuza = { 117, 118, 169, 186, 120, 123 };

		// Colors
		color color_aztecas = { 255, 0, 255, 255 };
		color color_ballas	= { 255, 255, 0, 255 };
		color color_rifa	= { 255, 122, 103, 238 };
		color color_vagos	= { 255, 255, 255, 0 };
		color color_grove	= { 255, 0, 255, 0 };

		color color_lcn	   = { 255, 84, 25, 25 };
		color color_rm	   = { 255, 25, 49, 84 };
		color color_yakuza = { 255, 255, 0, 0 };

		NLOHMANN_DEFINE_TYPE_INTRUSIVE(struct data,
									   act_key, menu_key, disable_samp_nametags, clear_screenshot,
									   skins_aztecas, skins_ballas, skins_rifa, skins_vagos, skins_grove,
									   skins_lcn, skins_rm, skins_yakuza,
									   color_aztecas, color_ballas, color_rifa, color_vagos, color_grove,
									   color_lcn, color_rm, color_yakuza);
	};

private:
	config();

public:
	config(const config &)			  = delete;
	config &operator=(const config &) = delete;
	~config();

	static config &instance();

	void load();
	void save() const;

	struct data		  *operator->();
	const struct data *operator->() const;

private:
	struct data data_;
};

#endif // CONFIG_HPP
