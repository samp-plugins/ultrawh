#ifndef PLUGIN_HPP
#define PLUGIN_HPP

#include <cstring>
#include <tuple>
#include <lemon/detour.hpp>
#include <lemon/hook.hpp>
#include "dx9.hpp"
#include "menu.hpp"
#include "prototypes.hpp"
#include "wallhack.hpp"

class plugin {
	lemon::detour<wndproc_t> wndproc_hook_{ 0x747EB0 };
	lemon::hook<>			 present_hook_{ dx9::vt_func_addr(17) };
	lemon::hook<>			 reset_hook_{ dx9::vt_func_addr(16) };

	std::shared_ptr<wallhack> wh_;
	std::shared_ptr<menu>	  menu_;

	lemon::detour<take_screenshot_t> take_screenshot_hook_{ 0x70FC0, "samp.dll" };
	bool							 screenshot_ = false;

public:
	plugin();
	~plugin();

private:
	LRESULT wndproc(wndproc_t orig, HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);

	void on_present();
	void on_reset();

	void take_screenshot(take_screenshot_t orig);
};

#endif // PLUGIN_HPP
