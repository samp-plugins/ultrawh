#ifndef SAMP_LIB_HPP
#define SAMP_LIB_HPP

#include <cstdint>

namespace samp {
	enum class ver : int { unknown = 0,
						   r1 = 1,
						   r3 = 2 };

	[[nodiscard]] std::uintptr_t lib();
	[[nodiscard]] std::uintptr_t ep();

	[[nodiscard]] enum ver ver();

	[[nodiscard]] bool is_r1();
	[[nodiscard]] bool is_r3();
} // namespace samp

#endif // SAMP_LIB_HPP
