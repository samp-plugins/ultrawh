#ifndef SAMP_REMOTE_PLAYER_HPP
#define SAMP_REMOTE_PLAYER_HPP

#include <cstdint>
#include <string>
#include <tuple>

namespace samp {
	class remote_player {
	public:
		remote_player(void *remote_player_ptr);
		~remote_player();

		[[nodiscard]] void *ptr() const;

		[[nodiscard]] void	   *&data();
		[[nodiscard]] std::string name();

		[[nodiscard]] void		   *&samp_ped();
		[[nodiscard]] std::uint16_t &id();
		[[nodiscard]] float			&health();
		[[nodiscard]] float			&armour();

		[[nodiscard]] void *&gta_ped();

		[[nodiscard]] std::tuple<float, float, float> pos();
		[[nodiscard]] std::uint16_t					&skin();

	private:
		void *remote_player_ptr_ = nullptr;

		void **remote_player_data_ = nullptr;
		void	 *remote_player_name_ = nullptr;
	};
} // namespace samp

#endif // SAMP_REMOTE_PLAYER_HPP
