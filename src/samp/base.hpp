#ifndef SAMP_BASE_HPP
#define SAMP_BASE_HPP

#include <cstdint>
#include <windows.h>

namespace samp {
	[[nodiscard]] bool inited();

	class base {
		base();
		~base();

	public:
		base(const base &)			  = delete;
		base &operator=(const base &) = delete;

		static base &instance();

		[[nodiscard]] void *ptr() const;

		[[nodiscard]] int			  &game_state();
		[[nodiscard]] char		   *&server_addr();
		[[nodiscard]] std::uint16_t &server_port();
		[[nodiscard]] bool		   &nametag_status();
		[[nodiscard]] void		   *&settings_ptr();
		[[nodiscard]] void		   *&pools_ptr();

		[[nodiscard]] class pools &pools();

	private:
		void *samp_ = nullptr;

		int			*game_state_	   = nullptr;
		char			 *server_addr_	   = nullptr;
		std::uint16_t *server_port_	   = nullptr;
		bool			 *nametag_status_ = nullptr;
		void		 **settings_ptr_   = nullptr;
		void		 **pools_ptr_	   = nullptr;

		class pools *pools_ = nullptr;
	};
} // namespace samp

#endif // SAMP_BASE_HPP
