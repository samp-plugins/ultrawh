#include "pools.hpp"
#include "lib.hpp"
#include "player_pool.hpp"

namespace samp::offsets {
	// { unknown, r1, r3 }
	constexpr std::uintptr_t object[]	= { 0, 0x4, 0x14 };
	constexpr std::uintptr_t textdraw[] = { 0, 0x10, 0x20 };
	constexpr std::uintptr_t player[]	= { 0, 0x18, 0x8 };
	constexpr std::uintptr_t vehicle[]	= { 0, 0x1C, 0xC };
	constexpr std::uintptr_t pickup[]	= { 0, 0x20, 0x10 };
} // namespace samp::offsets

samp::pools::pools(void *pools_ptr) :
	pools_ptr_(pools_ptr) {
	auto version	= static_cast<int>(ver());
	auto pools_addr = reinterpret_cast<std::uintptr_t>(pools_ptr_);

	object_ptr_	  = reinterpret_cast<void **>(pools_addr + offsets::object[version]);
	textdraw_ptr_ = reinterpret_cast<void **>(pools_addr + offsets::textdraw[version]);
	player_ptr_	  = reinterpret_cast<void **>(pools_addr + offsets::player[version]);
	vehicle_ptr_  = reinterpret_cast<void **>(pools_addr + offsets::vehicle[version]);
	pickup_ptr_	  = reinterpret_cast<void **>(pools_addr + offsets::pickup[version]);

	player_pool_ = new samp::player_pool(player_ptr());
}

samp::pools::~pools() {
	delete player_pool_;
}

void *samp::pools::ptr() const {
	return pools_ptr_;
}

void *&samp::pools::object_ptr() {
	return *object_ptr_;
}

void *&samp::pools::textdraw_ptr() {
	return *textdraw_ptr_;
}

void *&samp::pools::player_ptr() {
	return *player_ptr_;
}

void *&samp::pools::vehicle_ptr() {
	return *vehicle_ptr_;
}

void *&samp::pools::pickup_ptr() {
	return *pickup_ptr_;
}

samp::player_pool &samp::pools::player_pool() {
	return *player_pool_;
}