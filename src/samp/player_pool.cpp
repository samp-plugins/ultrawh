#include "player_pool.hpp"
#include <stdexcept>
#include "lib.hpp"
#include "remote_player.hpp"

namespace samp::offsets {
	// { unknown, r1, r3 }
	constexpr std::uintptr_t local_player_id[]	  = { 0, 0x4, 0x2F1C };
	constexpr std::uintptr_t local_player_nick[]  = { 0, 0xA, 0x2F22 };
	constexpr std::uintptr_t local_player_ping[]  = { 0, 0x26, 0x2F14 };
	constexpr std::uintptr_t local_player_score[] = { 0, 0x2A, 0x2F18 };

	constexpr std::uintptr_t is_listed[]		  = { 0, 0xFDE, 0xFB4 };
	constexpr std::uintptr_t remote_players_ptr[] = { 0, 0x2E, 0x4 };
} // namespace samp::offsets

samp::player_pool::player_pool(void *player_ptr) :
	player_ptr_(player_ptr) {
	auto version	 = static_cast<int>(ver());
	auto player_addr = reinterpret_cast<std::uintptr_t>(player_ptr_);

	local_player_id_	= reinterpret_cast<std::uint16_t *>(player_addr + offsets::local_player_id[version]);
	local_player_name_	= reinterpret_cast<void *>(player_addr + offsets::local_player_nick[version]);
	local_player_ping_	= reinterpret_cast<int *>(player_addr + offsets::local_player_ping[version]);
	local_player_score_ = reinterpret_cast<int *>(player_addr + offsets::local_player_score[version]);

	is_listed_			= reinterpret_cast<int *>(player_addr + offsets::is_listed[version]);
	remote_players_ptr_ = reinterpret_cast<void **>(player_addr + offsets::remote_players_ptr[version]);
}

samp::player_pool::~player_pool() = default;

void *samp::player_pool::ptr() const {
	return player_ptr_;
}

std::uint16_t &samp::player_pool::local_player_id() {
	return *local_player_id_;
}

std::string samp::player_pool::local_player_name() {
	struct msvc_string {
		union {
			char  szString[0x10];
			char *pszString;
		};
		std::size_t length;
		std::size_t allocated;
	};
	auto string = *reinterpret_cast<msvc_string *>(local_player_name_);
	if (string.allocated < 0x10) return { string.szString, string.length };
	return { string.pszString, string.length };
}

int &samp::player_pool::local_player_ping() {
	return *local_player_ping_;
}

int &samp::player_pool::local_player_score() {
	return *local_player_score_;
}

std::tuple<float, float, float> samp::player_pool::local_player_pos() {
	auto gta_ped_addr = *reinterpret_cast<std::uintptr_t *>(0xB6F5F0);
	if (!gta_ped_addr) return std::make_tuple(0.f, 0.f, 0.f);
	auto matrix_addr = *reinterpret_cast<std::uintptr_t *>(gta_ped_addr + 0x14);
	auto x			 = *reinterpret_cast<float *>(matrix_addr + 0x30);
	auto y			 = *reinterpret_cast<float *>(matrix_addr + 0x34);
	auto z			 = *reinterpret_cast<float *>(matrix_addr + 0x38);
	return std::make_tuple(x, y, z);
}

int &samp::player_pool::is_listed(int id) {
	if (id < 0 || id >= max_player) {
		throw std::runtime_error("player_pool: invalid player id");
	}
	return is_listed_[id];
}

samp::remote_player samp::player_pool::remote_player(int id) {
	if (id < 0 || id >= max_player) {
		throw std::runtime_error("player_pool: invalid player id");
	}
	return { remote_players_ptr_[id] };
}

bool samp::player_pool::exists(int id) {
	if (is_listed(id) != 1) return false;
	auto ped = remote_player(id);
	if (!ped.ptr()) return false;
	if (!ped.data()) return false;
	if (!ped.samp_ped()) return false;
	if (!ped.gta_ped()) return false;
	return true;
}

samp::remote_player samp::player_pool::operator[](int id) {
	return remote_player(id);
}
