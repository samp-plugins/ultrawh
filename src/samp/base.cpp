#include "base.hpp"
#include <stdexcept>
#include "lib.hpp"
#include "pools.hpp"

namespace samp::offsets {
	// { unknown, r1, r3 }
	constexpr std::uintptr_t samp_info[] = { 0, 0x21A0F8, 0x26E8DC };

	constexpr std::uintptr_t game_state[]	  = { 0, 0x3BD, 0x3CD };
	constexpr std::uintptr_t server_addr[]	  = { 0, 0x20, 0x30 };
	constexpr std::uintptr_t server_port[]	  = { 0, 0x225, 0x235 };
	constexpr std::uintptr_t nametag_status[] = { 0, 0x217, 0x234 };

	constexpr std::uintptr_t settings[] = { 0, 0x3C5, 0x3D5 };
	constexpr std::uintptr_t pools[]	= { 0, 0x3CD, 0x3DE };
} // namespace samp::offsets

bool samp::inited() {
	auto library = lib();
	if (!library) return false;

	auto samp_info = lib() + offsets::samp_info[static_cast<int>(ver())];
	return samp_info && *reinterpret_cast<std::uintptr_t *>(samp_info);
}

samp::base::base() {
	if (ver() == ver::unknown) throw std::runtime_error("unsupported version or samp is not loaded");

	auto version   = static_cast<int>(samp::ver());
	samp_		   = *reinterpret_cast<void **>(lib() + offsets::samp_info[version]);
	auto samp_addr = reinterpret_cast<std::uintptr_t>(samp_);

	game_state_		= reinterpret_cast<int *>(samp_addr + offsets::game_state[version]);
	server_addr_	= reinterpret_cast<char *>(samp_addr + offsets::server_addr[version]);
	server_port_	= reinterpret_cast<std::uint16_t *>(samp_addr + offsets::server_port[version]);
	nametag_status_ = reinterpret_cast<bool *>(samp_addr + offsets::nametag_status[version]);
	settings_ptr_	= reinterpret_cast<void **>(samp_addr + offsets::settings[version]);
	pools_ptr_		= reinterpret_cast<void **>(samp_addr + offsets::pools[version]);

	pools_ = new samp::pools(pools_ptr());
}

samp::base::~base() {
	delete pools_;
}

samp::base &samp::base::instance() {
	static base instance;
	return instance;
}

void *samp::base::ptr() const {
	return samp_;
}

int &samp::base::game_state() {
	return *game_state_;
}

char *&samp::base::server_addr() {
	return server_addr_;
}

std::uint16_t &samp::base::server_port() {
	return *server_port_;
}

bool &samp::base::nametag_status() {
	return *nametag_status_;
}

void *&samp::base::pools_ptr() {
	return *pools_ptr_;
}

void *&samp::base::settings_ptr() {
	return *settings_ptr_;
}

samp::pools &samp::base::pools() {
	return *pools_;
}