#include "lib.hpp"
#include <windows.h>

std::uintptr_t samp::lib() {
	static std::uintptr_t samp = 0;
	if (samp) return samp;

	samp = reinterpret_cast<std::uintptr_t>(GetModuleHandleA("samp.dll"));
	if (samp == -1) samp = 0;
	return samp;
}

std::uintptr_t samp::ep() {
	static std::uintptr_t ep = 0;
	if (ep) return ep;

	auto samp = lib();
	if (!samp) return 0;

	auto ntheader = reinterpret_cast<IMAGE_NT_HEADERS *>(samp + reinterpret_cast<IMAGE_DOS_HEADER *>(samp)->e_lfanew);
	ep			  = ntheader->OptionalHeader.AddressOfEntryPoint;
	return ep;
}

enum samp::ver samp::ver() {
	static auto ver = samp::ver::unknown;
	if (ver != samp::ver::unknown) return ver;

	switch (ep()) {
		case 0x31DF13:
			ver = ver::r1;
			break;
		case 0xCC4D0:
			ver = ver::r3;
			break;
	}
	return ver;
}

bool samp::is_r1() {
	return ver() == ver::r1;
}

bool samp::is_r3() {
	return ver() == ver::r3;
}
