#ifndef SAMP_PLAYER_POOL_HPP
#define SAMP_PLAYER_POOL_HPP

#include <cstdint>
#include <string>

namespace samp {
	class player_pool {
	public:
		static constexpr auto max_player = 1004;

		player_pool(void *player_ptr);
		~player_pool();

		[[nodiscard]] void *ptr() const;

		[[nodiscard]] std::uint16_t &local_player_id();
		[[nodiscard]] std::string	 local_player_name();
		[[nodiscard]] int			  &local_player_ping();
		[[nodiscard]] int			  &local_player_score();

		std::tuple<float, float, float> local_player_pos();

		[[nodiscard]] int				  &is_listed(int id);
		[[nodiscard]] class remote_player remote_player(int id);

		[[nodiscard]] bool exists(int id);

		[[nodiscard]] class remote_player operator[](int id);

	private:
		void *player_ptr_ = nullptr;

		std::uint16_t *local_player_id_	   = nullptr;
		void			 *local_player_name_  = nullptr;
		int			*local_player_ping_  = nullptr;
		int			*local_player_score_ = nullptr;

		int	*is_listed_		   = nullptr;
		void **remote_players_ptr_ = nullptr;
	};
} // namespace samp

#endif // SAMP_PLAYER_POOL_HPP
