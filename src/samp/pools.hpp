#ifndef SAMP_POOLS_HPP
#define SAMP_POOLS_HPP

#include <cstdint>

namespace samp {
	class pools {
	public:
		pools(void *pools_ptr);
		~pools();

		[[nodiscard]] void *ptr() const;

		[[nodiscard]] void *&object_ptr();
		[[nodiscard]] void *&textdraw_ptr();
		[[nodiscard]] void *&player_ptr();
		[[nodiscard]] void *&vehicle_ptr();
		[[nodiscard]] void *&pickup_ptr();

		[[nodiscard]] class player_pool &player_pool();

	private:
		void *pools_ptr_ = nullptr;

		void **object_ptr_	 = nullptr;
		void **textdraw_ptr_ = nullptr;
		void **player_ptr_	 = nullptr;
		void **vehicle_ptr_	 = nullptr;
		void **pickup_ptr_	 = nullptr;

		class player_pool *player_pool_ = nullptr;
	};
} // namespace samp

#endif // SAMP_POOLS_HPP
