#include "remote_player.hpp"
#include "lib.hpp"

namespace offsets {
	// { unknown, r1, r3 }
	constexpr std::uintptr_t remote_player_data[] = { 0, 0x0, 0x0 };
	constexpr std::uintptr_t remote_player_name[] = { 0, 0xC, 0xC };

	// From data
	constexpr std::uintptr_t remote_player_samp_ped[] = { 0, 0x0, 0x0 };
	constexpr std::uintptr_t remote_player_id[]		  = { 0, 0xAB, 0x8 };
	constexpr std::uintptr_t remote_player_health[]	  = { 0, 0x1BC, 0x1B0 };
	constexpr std::uintptr_t remote_player_armour[]	  = { 0, 0x1B8, 0x1AC };

	// From samp ped
	constexpr std::uintptr_t remote_player_gta_ped[] = { 0, 0x2A4, 0x2A4 };
} // namespace offsets

samp::remote_player::remote_player(void *remote_player_ptr) :
	remote_player_ptr_(remote_player_ptr) {
	auto version			= static_cast<int>(ver());
	auto remote_player_addr = reinterpret_cast<std::uintptr_t>(remote_player_ptr_);

	remote_player_data_ = reinterpret_cast<void **>(remote_player_addr + offsets::remote_player_data[version]);
	remote_player_name_ = reinterpret_cast<void *>(remote_player_addr + offsets::remote_player_name[version]);
}

samp::remote_player::~remote_player() = default;

void *samp::remote_player::ptr() const {
	return remote_player_ptr_;
}

void *&samp::remote_player::data() {
	return *remote_player_data_;
}

std::string samp::remote_player::name() {
	struct msvc_string {
		union {
			char  szString[0x10];
			char *pszString;
		};
		size_t length;
		size_t allocated;
	};
	auto string = *reinterpret_cast<msvc_string *>(remote_player_name_);
	if (string.allocated < 0x10) return { string.szString, string.length };
	return { string.pszString, string.length };
}

void *&samp::remote_player::samp_ped() {
	auto data_addr = reinterpret_cast<std::uintptr_t>(data());
	auto version   = static_cast<int>(ver());
	return *reinterpret_cast<void **>(data_addr + offsets::remote_player_samp_ped[version]);
}

std::uint16_t &samp::remote_player::id() {
	auto data_addr = reinterpret_cast<std::uintptr_t>(data());
	auto version   = static_cast<int>(ver());
	return *reinterpret_cast<std::uint16_t *>(data_addr + offsets::remote_player_id[version]);
}

float &samp::remote_player::health() {
	auto data_addr = reinterpret_cast<std::uintptr_t>(data());
	auto version   = static_cast<int>(ver());
	return *reinterpret_cast<float *>(data_addr + offsets::remote_player_health[version]);
}

float &samp::remote_player::armour() {
	auto data_addr = reinterpret_cast<std::uintptr_t>(data());
	auto version   = static_cast<int>(ver());
	return *reinterpret_cast<float *>(data_addr + offsets::remote_player_armour[version]);
}

void *&samp::remote_player::gta_ped() {
	auto samp_ped_addr = reinterpret_cast<std::uintptr_t>(samp_ped());
	auto version	   = static_cast<int>(ver());
	return *reinterpret_cast<void **>(samp_ped_addr + offsets::remote_player_gta_ped[version]);
}

std::tuple<float, float, float> samp::remote_player::pos() {
	auto gta_ped_addr = reinterpret_cast<std::uintptr_t>(gta_ped());
	auto matrix_addr  = *reinterpret_cast<std::uintptr_t *>(gta_ped_addr + 0x14);
	auto x			  = *reinterpret_cast<float *>(matrix_addr + 0x30);
	auto y			  = *reinterpret_cast<float *>(matrix_addr + 0x34);
	auto z			  = *reinterpret_cast<float *>(matrix_addr + 0x38);
	return std::make_tuple(x, y, z);
}

std::uint16_t &samp::remote_player::skin() {
	auto gta_ped_addr = reinterpret_cast<std::uintptr_t>(gta_ped());
	return *reinterpret_cast<std::uint16_t *>(gta_ped_addr + 0x22);
}
