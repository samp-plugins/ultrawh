#include "misc.hpp"
#include <d3dx9.h>
#include <lemon/mem.hpp>

vector misc::get_bone_pos(void *ped, int bone_id) {
	using get_bone_pos_t = void(__thiscall *)(void *, vector *, int, bool);

	vector pos;
	reinterpret_cast<get_bone_pos_t>(0x5E01C0)(ped, &pos, bone_id, true);
	return pos;
}

vector misc::world_to_screen_pos(const vector &world_pos) {
	vector screen_pos;

	// Get the static view matrix as D3DXMATRIX
	D3DXMATRIX matrix(reinterpret_cast<float *>(0xB6FA2C));

	// Get the static virtual screen (x,y)-sizes
	auto len_x = *reinterpret_cast<DWORD *>(0xC17044);
	auto len_y = *reinterpret_cast<DWORD *>(0xC17048);

	// Do a transformation
	screen_pos.x = (world_pos.z * matrix._31) + (world_pos.y * matrix._21) + (world_pos.x * matrix._11) + matrix._41;
	screen_pos.y = (world_pos.z * matrix._32) + (world_pos.y * matrix._22) + (world_pos.x * matrix._12) + matrix._42;
	screen_pos.z = (world_pos.z * matrix._33) + (world_pos.y * matrix._23) + (world_pos.x * matrix._13) + matrix._43;

	// Get the correct screen coordinates
	auto fRecip = 1.0 / screen_pos.z; //(vecScreen->z - (*dwLenZ));
	screen_pos.x *= static_cast<float>(fRecip * len_x);
	screen_pos.y *= static_cast<float>(fRecip * len_y);

	return screen_pos;
}

D3DCOLOR misc::samp_clist_color(int id) {
	static constexpr std::uintptr_t offset[] = { 0, 0x216378, 0x151578 };

	auto lib = samp::lib();
	if (!lib) return 0;

	auto ver = static_cast<int>(samp::ver());
	return (reinterpret_cast<DWORD *>(lib + offset[ver])[id] >> 8) | 0xFF000000;
}

D3DCOLOR misc::argb_to_abgr(D3DCOLOR argb) {
	auto a = (argb >> 24) & 0xFF;
	auto r = (argb >> 16) & 0xFF;
	auto g = (argb >> 8) & 0xFF;
	auto b = argb & 0xFF;
	return (a << 24) | (b << 16) | (g << 8) | r;
}

std::tuple<float, float> misc::screen_res() {
	static constexpr auto screen_res_addr = 0xC9C040u;
	const auto			  width			  = *reinterpret_cast<int *>(screen_res_addr);
	const auto			  height		  = *reinterpret_cast<int *>(screen_res_addr + 0x4);
	return std::make_tuple(static_cast<const float>(width), static_cast<const float>(height));
}

void misc::toggle_nicks(bool status) {
	static constexpr std::uintptr_t nametag_offset[] = { 0, 0x70D40, 0x74C30 };
	static constexpr std::uintptr_t bars_offset[]	 = { 0, 0x6FC30, 0x73B20 };

	auto lib = samp::lib();
	if (!lib) return;

	auto ver = static_cast<int>(samp::ver());
	lemon::mem::safe_copy(lib + nametag_offset[ver], reinterpret_cast<std::uintptr_t>(status ? "\x55" : "\xC3"), 1);
	lemon::mem::safe_copy(lib + bars_offset[ver], reinterpret_cast<std::uintptr_t>(status ? "\x55" : "\xC3"), 1);
}